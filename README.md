# Ultra Low Power Trigger Sensors Version 3

A whole system with battery-based, real-time trigger sensors to monitor doors, windows, motion and climate with a central hub and <240ms trigger time. 
#ESP8266 #Attiny #ESPNOW.
Version 3

## Buy 

- The Hub3 - https://store.mrdiy.ca/p/thehub_ver3
- The Contact Sensor - https://store.mrdiy.ca/p/wireless-contact-sensor
- The Climate Sensor - https://store.mrdiy.ca/p/wireless-climate-sensor

## Videos

#### The Hub 3

[![MrDIY Ultra Low Power Hub3 YouTube video](https://img.youtube.com/vi/as6laOtSeNY/0.jpg)](https://youtu.be/as6laOtSeNY)


#### Wireless Contact Sensor

[![MrDIY Wireless Contact Sensor YouTube video](https://img.youtube.com/vi/Oi3RwFTchig/0.jpg)](https://youtu.be/Oi3RwFTchig)

#### Wireless Climate Sensor

[![MrDIY Wireless Climate Sensor YouTube video](https://img.youtube.com/vi/QyQNQsqEXCI/0.jpg)](https://youtu.be/QyQNQsqEXCI)

## Schematics

#### The Hub 3 

![The Hub 3 Schematic](https://gitlab.com/MrDIYca/ultra-low-power-trigger-sensors-version-3/-/raw/main/schematic_hub.png)

#### Contact Sensor 

![Contact Sensor Schematic](https://gitlab.com/MrDIYca/ultra-low-power-trigger-sensors-version-3/-/raw/main/schematic_contact_sensor.png)

#### Climate Sensor 

![Climate Sensor Schematic](https://gitlab.com/MrDIYca/ultra-low-power-trigger-sensors-version-3/-/raw/main/schematic_climate_sensor.png)

## Enclosures (STL)
Hub

![STL file](https://gitlab.com/MrDIYca/ultra-low-power-trigger-sensors-version-3/-/raw/main/img/enclosure_preview.png)

Climate

![STL file](https://gitlab.com/MrDIYca/ultra-low-power-trigger-sensors-version-3/-/raw/main/img/enclosure_climate_preview.png)


## Thanks
Many thanks to all the authors and contributors to the open source libraries and community. You are the reason this project exists.

